# tppi

Test project - please ignore

Prototype of test system for python and bash scripts.

If you want just to run some example test, there're two prepared environments in `example` branch. Refer to ['HOW TO TRY'](#markdown-header-how-to-try) section.

If you want to create environment from the scratch, refer to [Prepare the testing environment](#markdown-header-prepare-the-testing-environment) section.


# HOW TO TRY
Before you start, be sure to install docker, python (tested with python-3.7) and its venv module (or similar).
This does not require to create environment, it uses already prepared ones.
These environments already contain example tests listed below.

At first you need to achieve working copy of prepared testing system.

* Clone repo and checkout to branch example and update submodules:
```
git clone https://bitbucket.org/Ayajuaska/tppi.git
cd tppi
git checkout example
git submodule init
git submodule update
```
NB `example` is the actual branch with example tests.

* create virtual environment and activate it (optional):
```
    python3 -m venv <path>
    source <path>/bin/activate
```

* Install python requirements (if needed):
```
    pip install -r <path to clonned repo>/requirements.txt
```

At this step we've got our copy of test system with example environments and installed requirements,
but before we start running tests, we need to create database structure and start the daemon: 

* Create sample database's schema:
```
    python -m tppi --create-schema
```

* Start the daemon and wait until docker images will be ready:
```
    python -m tppi -s
```

It could take some time to build a docker image.

Now we're ready to run sample tests:

* Run sample tests:
```
    python -m tppi -t tasty_test.py -e tasty/alpha
    python -m tppi -t test_a -e tasty/beta
```

* Watch results:
```
    python -m tppi -l
```
Logs will be in `./log/<run_id>_<env_name>_<test_name>` directory.
Daemon's log will be in `./log`

### RUN

* Prepare database:
```
    python -m tppi --create-schema
```

* Start the daemon: 
```
    python -m tppi -s
```
It will build registered docker images and detach from console.

* Schedule/start test:
```
    python -m tppi -e <env-name> -t <test-name>
```
It will send command to daemon to run docker container with environment and enqueue test.
If environment is in use, it will add task to queue and later it will be run.

* Get tests status:
```
    python -m tppi -l
```
It will show all tests' status

* Stop the daemon:
```
    python -m tppi -q
```
It will stop daemon


## KNOWN ISSUES
* In some cases client can hang due lack of daemon's status checks
* Unittests coverage (due lack of time) :(
* Little bit complicated and rigid configuration

# Prepare the testing environment

Here described how to create you own testing environment.


* Create directory for your new environment

* Create config for test runner, for example
```
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# python or bash - type of testing
TEST_TYPE = 'bash'
# path to list of available tests
TEST_SET = 'tests.set'
# path to application 
SUBJECT_DIR = BASE_DIR
# path to tests
TESTS_DIR = '/tests'
```

* Create test set, for example add one name:
```
test_a
```

* Create Dockerfile with following content if you already have docker image for you app:
```
FROM <your_image>

RUN git clone -b dev https://bitbucket.org/Ayajuaska/tppi_tasty.git /test_runner
COPY tests.set /test_runner/
COPY config.py /test_runner/tasty/
WORKDIR /test_runner
RUN pip install -r requirements.txt
```

 or add these lines to the end of you'r app Dockerfile:
  
```
RUN git clone -b dev https://bitbucket.org/Ayajuaska/tppi_tasty.git /test_runner
COPY tests.set /test_runner/
COPY config.py /test_runner/tasty/
WORKDIR /test_runner
RUN pip install -r requirements.txt
```

* Check paths to the app itself and its tests once again in the config.py.

* Modify config in tppi.config, add your environment to `ENV` dict
```
ENV = {
    # Environment's name
    'beta': {
        # docker image tag
        'image': 'tasty/beta',
        # path to the environment, where the Dockerfile is
        # write here actual path to the environment
        'path': os.path.join(BASE_DIR, 'env/beta')
    },
}
```

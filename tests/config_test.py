import unittest
from tppi import config


class ConfigTest(unittest.TestCase):
    def test_basic(self):
        databases = config.DATABASES
        self.assertTrue('default' in databases)


if __name__ == '__main__':
    unittest.main()

import os
import pickle
import subprocess
import unittest
from time import sleep

import zmq

from tppi.config import CMD_SOCKET_PROTO, CMD_SOCKET_ADDR, CMD_SOCKET_PORT
from tppi.config import STATUS_SOCKET_PROTO, STATUS_SOCKET_ADDR, STATUS_SOCKET_PORT

cmd_socket_addr = '%s://%s:%s' % (CMD_SOCKET_PROTO, CMD_SOCKET_ADDR, CMD_SOCKET_PORT)
status_socket_addr = '%s://%s:%s' % (STATUS_SOCKET_PROTO,
                                     STATUS_SOCKET_ADDR.replace('*', '127.0.0.1'), STATUS_SOCKET_PORT)


class SocketsTest(unittest.TestCase):
    def setUp(self) -> None:
        with open('/tmp/tppi.pid', 'r') as pid_file:
            subprocess.call(['python3', '-m', 'tppi.daemon'])
            self.pid = int(pid_file.read())
        pass

    def tearDown(self) -> None:
        # noinspection PyBroadException
        try:
            os.kill(self.pid, 9)
        except BaseException:
            pass

    def test_quit(self):
        try:
            os.kill(self.pid, 0)
            exists = True
        except OSError:
            exists = False
        self.assertTrue(exists)

        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        socket.connect(cmd_socket_addr)
        socket.send(pickle.dumps({'command': 'quit'}))
        socket.close()
        sleep(1)

        try:
            os.kill(self.pid, 0)
            print('pid = %s exists' % self.pid)
            exists = True
        except OSError:
            exists = False
        self.assertFalse(exists)

    @staticmethod
    def test_status():
        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        socket.connect(status_socket_addr)
        socket.send(pickle.dumps({'status': '42'}))
        socket.close()


if __name__ == '__main__':
    unittest.main()

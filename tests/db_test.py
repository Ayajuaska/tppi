import unittest
from tppi.models import Runs
from tppi.models.test_runs import TestStatus as Status
from tppi.config import LOG_DIR

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class DbTestCase(unittest.TestCase):

    def setUp(self):
        self.engine = create_engine('sqlite:///:memory:', echo=False)
        session = sessionmaker(bind=self.engine)
        self.session = session()
        Runs.metadata.create_all(self.engine)
        random_test = Runs('sample_env', __name__, LOG_DIR)
        self.session.add(random_test)
        self.session.commit()

    def tearDown(self):
        Runs.metadata.drop_all(self.engine)

    def test_select(self):
        for row in self.session.query(Runs):
            self.assertEqual(row.environment, 'sample_env')
            self.assertEqual(row.test, __name__)
            self.assertEqual(Status(row.status), Status.PENDING)

    def test_count(self):
        self.assertEqual(self.session.query(Runs).count(), 1)


if __name__ == '__main__':
    unittest.main()

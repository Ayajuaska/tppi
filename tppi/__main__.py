import sys
from optparse import OptionParser, OptionGroup

from sqlalchemy import create_engine

from tppi.client import Client
from tppi.config import DATABASES
from tppi.models import Runs


def make_parser():
    parser = OptionParser()

    parser.add_option('--create-schema', None, dest='action', help='Prepare database structure',
                      const='create_schema', action='store_const')

    daemon_group = OptionGroup(parser, "Daemon control options")
    parser.add_option_group(daemon_group)
    daemon_group.add_option('-s', '--start', dest='action', const='start',
                            help='Start daemon', action='store_const')
    daemon_group.add_option('-k', '--kill', dest='action', const='kill',
                            help='Kill daemon', action='store_const')
    daemon_group.add_option('-q', '--quit', dest='action', const='quit',
                            help='Kill daemon', action='store_const')

    test_group = OptionGroup(parser, "Test control options")
    parser.add_option_group(test_group)
    test_group.add_option('-t', '--test', dest='test_name', help='Test name', action='store')
    test_group.add_option('-e', '--env', dest='env_name', help='Environment name', action='store')
    test_group.add_option('-l', '--list', dest='action', help='List tests status', const='list', action='store_const')
    return parser


def main(*args):
    client = Client()
    options, _ = make_parser().parse_args(list(args))
    if options.action:
        if options.action == 'start':
            print('Stating daemon')
            client.start_daemon()

        if options.action == 'quit':
            print('Stoping daemon')
            client.stop_daemon()

        if options.action == 'kill':
            print('Stoping daemon')
            client.kill_daemon()

        if options.action == 'list':
            print('Tests status:')
            client.status()

        if options.action == 'create_schema':
            engine = create_engine('%s:///%s' % (DATABASES['default']['ENGINE'], DATABASES['default']['NAME']))
            Runs.metadata.drop_all(engine)
            Runs.metadata.create_all(engine)
    else:
        if any((options.test_name, options.env_name)):
            if not all((options.test_name, options.env_name)):
                print('If you want to enqueue some test you have to set *BOTH* env and test name')
                return -1
            client.enqueue_test(env=options.env_name, test=options.test_name)


if __name__ == '__main__':
    main(*sys.argv)

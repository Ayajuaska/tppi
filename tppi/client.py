import logging
import os
import pickle

import zmq
from prettytable import PrettyTable

from tppi.config import CMD_SOCKET_PROTO, CMD_SOCKET_ADDR, CMD_SOCKET_PORT
from tppi.config import DAEMON_PID_FILE
from tppi.daemon import daemonize
from tppi.models import Runs, get_new_session

cmd_socket_addr = '%s://%s:%s' % (CMD_SOCKET_PROTO, CMD_SOCKET_ADDR, CMD_SOCKET_PORT)


class SocketContextManager:
    def __init__(self, addr):
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        self.socket.setsockopt(zmq.SNDTIMEO, 500)
        self.addr = addr

    def __enter__(self):
        """
        Connect to daemon
        """
        self.socket.connect(self.addr)
        return self.socket

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Close connection
        """
        self.socket.close(linger=5)


class Client:

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    def status(self):
        """
        List all tests status
        """
        session = get_new_session()
        query = session.query(Runs)
        if query.count() == 0:
            print('No running, pending or finished tests')
            return
        table = PrettyTable()
        table.field_names = ['id', 'test', 'environment', 'created_at', 'started_at', 'finished_at', 'status', 'logs']
        for row in query.all():
            self.logger.debug(row)
            table.add_row([row.as_dict()[k] for k in table.field_names])
        session.close()
        print(table)

    def enqueue_test(self, env, test):
        """
        Enqueue new test
        :param env: environment name
        :param test: test name
        """
        with SocketContextManager(cmd_socket_addr) as socket:
            socket.send(pickle.dumps({'command': 'enqueue', 'env': env, 'test': test}))
        pass

    def start_daemon(self):
        """
        Start daemon
        """
        pid = daemonize()
        self.logger.info('Daemon started with pid = %s', pid)

    def stop_daemon(self):
        """
        Stop daemon
        """
        with SocketContextManager(cmd_socket_addr) as socket:
            socket.send(pickle.dumps({'command': 'quit'}))
        self.logger.info('Command was sent')

    def kill_daemon(self):
        """
        Kill daemon without mercy
        """
        self.stop_daemon()
        if not os.path.isfile(DAEMON_PID_FILE):
            self.logger.info('Daemon is not running')
            return
        with open(DAEMON_PID_FILE, 'r') as pid_file:
            os.kill(int(pid_file.read()), 9)

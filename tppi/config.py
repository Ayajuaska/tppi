import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

LOG_DIR = os.path.join(BASE_DIR, 'log')

# Database configurations
# There can be more than one database
DATABASES = {
    'default': {
        'ENGINE': 'sqlite',
        'NAME': os.path.join(BASE_DIR, "db.sqlite3"),
    }
}

CMD_SOCKET_PROTO = 'tcp'
CMD_SOCKET_ADDR = '127.0.0.1'
CMD_SOCKET_PORT = 5555

STATUS_SOCKET_PROTO = 'tcp'
STATUS_SOCKET_ADDR = '*'
STATUS_SOCKET_PORT = 8888

DAEMON_PID_FILE = os.path.join(BASE_DIR, 'daemon.pid')

DAEMON_LOG_FILE = os.path.join(LOG_DIR, 'daemon.log')

ENV = {
    'alpha': {
        'image': 'tasty/alpha',
        'path': os.path.join(BASE_DIR, 'env/alpha')
    },
}

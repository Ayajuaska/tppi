import datetime
import logging
import os
import pickle
import shutil
import sys
import threading
from queue import Queue, Empty

import docker
import zmq
from docker.errors import NotFound
from zmq.error import ZMQError, Again

from tppi.config import CMD_SOCKET_PROTO, CMD_SOCKET_ADDR, CMD_SOCKET_PORT
from tppi.config import DAEMON_LOG_FILE
from tppi.config import DAEMON_PID_FILE
from tppi.config import ENV
from tppi.config import LOG_DIR
from tppi.config import STATUS_SOCKET_PROTO, STATUS_SOCKET_ADDR, STATUS_SOCKET_PORT
from tppi.models import Runs, get_new_session
from tppi.models.test_runs import TestStatus


class Daemon:
    commands_socket: zmq.Socket
    status_socket: zmq.Socket
    commands_thread: threading.Thread
    status_thread: threading.Thread
    handler_thread: threading.Thread

    cmd_socket_addr = '%s://%s:%s' % (CMD_SOCKET_PROTO, CMD_SOCKET_ADDR, CMD_SOCKET_PORT)
    status_socket_addr = '%s://%s:%s' % (STATUS_SOCKET_PROTO, STATUS_SOCKET_ADDR, STATUS_SOCKET_PORT)

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.commands_queue = Queue()
        self.running = False

        self.envs_available = set()
        self.containers = set()

    def prepare_envs(self):
        """
        Update env images
        """
        for name, env in ENV.items():
            self.logger.debug('Preparing %s env', name)
            print('Preparing %s ... ' % name, end='')
            client = docker.from_env()
            try:
                self.logger.debug('Path = %s', env['path'])
                img, logs = client.images.build(path=env['path'], tag=env['image'], rm=True)
                self.envs_available.add(env['image'])
                print('OK')
                for row in logs:
                    try:
                        row = row.get('stream')
                        if row and row.strip():
                            print('\t%s' % row.strip())
                    except:
                        pass

            except Exception as err:
                self.logger.error('Something went wrong at image build time: %s', err)
                print('Fail')
        self.logger.info('Prepared envs: %s', ', '.join(self.envs_available))
        print('Available environments: %s' % ', '.join(self.envs_available))

    def enqueue_test(self, env, name):
        """
        Put test to env's queue
        :param env: env name
        :param name: test name
        :return:
        """
        self.logger.debug('Adding test to queue: %s@%s', name, env)
        env_safe_name = env.replace('/', '_')
        new_test = Runs(environment=env, test=name,
                        logs='')
        container_name = '_'.join((env_safe_name, name))
        session = get_new_session()
        session.add(new_test)
        session.commit()
        session.query(Runs).filter(Runs.id == new_test.id).update(
            {'logs': os.path.join(LOG_DIR, '{}_{}_{}'.format(new_test.id, env_safe_name, name))}
        )
        self.logger.debug('%s', new_test.as_dict())
        if env not in self.envs_available:
            self.logger.debug('Test run failed due unknown env: %s not in %s', env, self.envs_available)
            try:
                session.query(Runs).filter(Runs.id == new_test.id).update(
                    {
                        'status': TestStatus.FAILED,
                        'finished_at': datetime.datetime.utcnow()
                    }
                )
            except Exception as err:
                self.logger.error('%s: %s', type(err), err)
            session.commit()
            session.close()
            return

        client = docker.from_env()
        net = client.networks.get('bridge')
        try:
            cont = client.containers.get(container_name)
            cont.exec_run(
                workdir='/test_runner/', cmd='python -m tasty --id %d %s' % (new_test.id, name),
                environment={'HOST': net.attrs['IPAM']['Config'][0]['Gateway']}
            )
        except NotFound:
            client.containers.run(
                image=env, command='python -m tasty --id %d %s' % (new_test.id, name),
                name=container_name, detach=True, auto_remove=True,
                environment={'HOST': net.attrs['IPAM']['Config'][0]['Gateway']}
            )
            new_test.started_at = datetime.datetime.utcnow()
            new_test.status = TestStatus.RUNNING
        except Exception as err:
            self.logger.error('%s: %s', type(err), err)
        session.add(new_test)
        session.commit()
        session.close()
        self.logger.debug('Test enqueued')

    def command_listener(self):
        """
        Read & enqueue command
        """
        self.logger.debug('Commands listener start')
        while self.running:
            try:
                raw_data = self.commands_socket.recv()
            except Again:
                continue
            except BaseException as err:
                self.logger.error('Error: %s (%s)', err, type(err))
                break
            if not raw_data:
                continue
            message = pickle.loads(raw_data)
            self.logger.debug('Received request: %s' % message)
            if not isinstance(message, dict):
                response = {
                    'status': 'error',
                    'message': 'Wrong command format: command must be a dict'
                }
                self.commands_socket.send(pickle.dumps(response))
                continue

            if 'command' not in message:
                response = {
                    'status': 'error',
                    'message': 'Empty command'
                }
                self.commands_socket.send(pickle.dumps(response))
                response = {
                    'status': 'error',
                    'message': 'Empty command'
                }
                self.commands_socket.send(pickle.dumps(response))
                continue

            if message['command'] == 'quit':
                response = {
                    'status': 'ok',
                    'message': 'Stopping...'
                }
                self.commands_socket.send(pickle.dumps(response))
                self.running = False
                break

            #  Send reply back to client
            self.commands_queue.put_nowait(message)
            response = {
                'status': 'ok',
                'message': 'Command enqueued'
            }
            self.commands_socket.send(pickle.dumps(response))
        self.logger.debug('Commands thread is finishing')

    def status_listener(self):
        """
        Receive status message from environments
        """
        self.logger.debug('Status listener start (%s)', self.running)
        while self.running:
            try:
                raw_data = self.status_socket.recv_pyobj()
                self.status_socket.send(b'OK')
                self.logger.debug('Raw data: %s', raw_data)
                if raw_data:
                    message = raw_data
                    self.logger.debug('Received request: %s' % message)
                    self.handle_status(message)

            except Again:
                continue
            except BaseException as err:
                self.logger.error('Error (%s): %s', type(err), err)
                break
        self.logger.debug('Status thread is finishing')

    def handle_status(self, message):
        self.logger.debug('Status message: %s', message)
        session = get_new_session()
        try:
            run: Runs = session.query(Runs).get(int(message['id']))
            run.status = TestStatus.SUCCESS if message['result'] == 0 else TestStatus.FAILED
            run.started_at = message['start_time']
            run.finished_at = message['finish_time']
            session.add(run)
            session.commit()

            subj_log_dir = run.logs
            if os.path.exists(subj_log_dir):
                shutil.rmtree(subj_log_dir, ignore_errors=True)
            os.makedirs(subj_log_dir)
            with open(os.path.join(subj_log_dir, 'error.log'), 'w') as out:
                out.write(message['errors'])
            with open(os.path.join(subj_log_dir, 'output.log'), 'w') as out:
                out.write(message['output'])
        except Exception as err:
            self.logger.error('%s: %s', type(err), err)
        finally:
            session.close()

    def command_handler(self):
        while self.running:
            try:
                cmd = self.commands_queue.get(timeout=1)
            except Empty:
                continue
            else:
                self.commands_queue.task_done()
                if cmd['command'] == 'enqueue':
                    try:
                        self.enqueue_test(cmd['env'], cmd['test'])
                    except KeyError as err:
                        self.logger.error('Key error in enqueue command: %s', err)
                    except Exception as err:
                        self.logger.error('%s: %s', type(err), err)
                if cmd['command'] == 'update':
                    self.prepare_envs()

    def run(self):
        """
        Daemon's main entry point
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.debug('Started with pid: %s', os.getpid())
        try:
            self.commands_thread = threading.Thread(target=self.command_listener)
            self.status_thread = threading.Thread(target=self.status_listener)
            self.handler_thread = threading.Thread(target=self.command_handler)
            context = zmq.Context()

            self.commands_socket = context.socket(zmq.REP)
            self.commands_socket.RCVTIMEO = 500
            self.commands_socket.bind(Daemon.cmd_socket_addr)

            self.status_socket = context.socket(zmq.REP)
            self.status_socket.RCVTIMEO = 500
            self.status_socket.bind(Daemon.status_socket_addr)

            self.running = True

            self.status_thread.start()
            self.commands_thread.start()
            self.handler_thread.start()

            if self.commands_thread.is_alive():
                self.commands_thread.join()

            if self.status_thread.is_alive():
                self.status_thread.join()

            if self.handler_thread.is_alive():
                self.handler_thread.join()

            self.logger.debug('Exiting')
        except BaseException as err:
            self.logger.error('Error: %s (%s)', err, type(err))
            exit(1)


def daemonize():
    """
    Run daemon in child process
    :return:
    """
    pid_file = DAEMON_PID_FILE
    logger = logging.getLogger(__name__)
    logging.basicConfig(filename=DAEMON_LOG_FILE, level=logging.DEBUG,
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    context = zmq.Context()
    for addr in (Daemon.cmd_socket_addr, Daemon.status_socket_addr):
        test_socket = context.socket(zmq.REP)
        try:
            test_socket.bind(addr)
        except ZMQError:
            print('It seems the Daemon is running now')
            return
        test_socket.close()

    daemon = Daemon()
    daemon.prepare_envs()
    try:
        pid = os.fork()
        if pid > 0:
            logger.warning('Pid = %s' % pid)
            return pid
    except OSError as err:
        sys.stderr.write('fork #2 failed: {0}\n'.format(err))
        sys.exit(1)

    os.setsid()
    # os.umask(0)
    sys.stdout.flush()
    sys.stderr.flush()
    si = open(os.devnull, 'r')
    se = open(os.devnull, 'a+')

    os.dup2(si.fileno(), sys.stdin.fileno())
    os.dup2(se.fileno(), sys.stderr.fileno())

    pid = str(os.getpid())
    with open(pid_file, 'w+') as f:
        f.write(pid + '\n')
    daemon.run()
    os.remove(pid_file)
    exit(0)


if __name__ == '__main__':
    daemonize()

from tppi.models.test_runs import Runs

from tppi.config import DATABASES
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def get_new_session():
    engine = create_engine('%s:///%s' % (DATABASES['default']['ENGINE'], DATABASES['default']['NAME']))
    session = sessionmaker(bind=engine)
    return session()
import datetime
from enum import IntEnum

from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class TestStatus(IntEnum):
    PENDING = 0
    RUNNING = 1
    SUCCESS = 2
    FAILED = 3

    def __repr__(self):
        return self.value

    def __str__(self):
        return self.name


class Runs(Base):
    __tablename__ = 'test_runs'
    id = Column(Integer, primary_key=True)
    environment = Column(String)
    test = Column(String)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    started_at = Column(DateTime, nullable=True)
    finished_at = Column(DateTime, nullable=True)
    status = Column(Integer)
    logs = Column(String)

    def __init__(self, environment, test, logs, **kwargs):
        self.environment = environment
        self.test = test
        self.logs = logs
        self.status = TestStatus.PENDING
        if 'created_at' in kwargs:
            self.created_at = kwargs['created_at']

    def fail(self):
        self.status = TestStatus.FAILED
        self.finished_at = datetime.datetime.utcnow

    def __repr__(self):
        return "<TestRuns('%s','%s', '%s')>" % (self.environment, self.test, TestStatus(self.status))

    def as_dict(self):
        return {
            'id': self.id,
            'environment': self.environment,
            'test': self.test,
            'created_at': self.created_at,
            'started_at': self.started_at or '',
            'finished_at': self.finished_at or '',
            'status': TestStatus(self.status) if self.status is not None else '',
            'logs': self.logs,
        }
